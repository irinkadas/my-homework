import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class ElementsFunctional {

    //Завдання 1
    //Створи список з 10 елементів і заповни його рандомними числами, потрібно вивести суму квадратів всіх елементів списку.
    // метод map () для зведення в квадрат кожного елемента,
    // метод reduce () для згортки всіх елементів в одне число.
    static void elementsAreSquared() {
        List<Integer> randomNumbers = Arrays.asList(1,7,5,6,2,9,3,4,10,8);

        Optional<Integer> sum = randomNumbers.stream()
                .map(s -> s*s)
                .reduce(Integer::sum);

        sum.ifPresent(System.out::println);
    }
    //Завдання 2
    //Для будь-якого набору випадково-згенерованих цілих чисел потрібно визначити кількість парних.
    // Використати засоби програмного інтерфейсу Stream API.
    static void evenNumbers(){
        ArrayList<Integer> AL = new ArrayList<Integer>();
        int number;
        Random rnd = new Random();

        for (int i=0; i<10; i++) {
            number = rnd.nextInt() % 100;
            AL.add(number);
        }

        System.out.println("Array AL:");
        System.out.println(AL);

        //  Визначити кількість парних чисел. Спосіб 1.
        Stream<Integer> st = AL.stream();
        Predicate<Integer> fn;

        fn = (n) -> (n%2) == 0;
        Stream<Integer> resStream = st.filter(fn);

        System.out.println("n = " + resStream.count());

        // 3. Визначити кількість парних чисел. Спосіб 2 - швидкий спосіб
        int n2 = (int)(AL.stream().filter((n)->(n%2)==0)).count();
        System.out.println("n2 = " + n2);
    }

    // Завдання 3
    //Задано набір прізвищ співробітників. Розроби програму, що показує всі прізвища, які починаються на букву ‘J’.
    // Використання програмного інтерфейсу Stream API.
    static void setSurnamesEmployee(){
    Scanner scanner = new Scanner(System.in);
    String s;
    ArrayList<String> AL = new ArrayList<String>();

    System.out.println("Enter names: ");
    while (true) {
        System.out.print("name = ");
        s = scanner.nextLine();
        if (s.equals(""))
            break;
        AL.add(s);
    }
    System.out.println();

    // 2. Вивести масив введених прізвищ
    System.out.println("AL = " + AL);

    // 3. Обчислити кількість прізвищ, що починаються на 'J'
    // 3.1. Оголосити посилання на функціональний інтерфейс Predicate<T>
    Predicate<String> fn;

    // 3.2. Встановити лямбда-вираз для fn
    fn = (str) -> {
        return str.charAt(0) == 'J';
    };

    // 3.3. Конвертувати AL в потік рядків
    Stream<String> stream = AL.stream();

    // 3.4. Отримати відфільтрований потік згідно предикату,
    // визначеному в fn()
    Stream<String> resStream = stream.filter(fn);

    // 3.5. Вивести кількість прізвищ, що починаються на J
    System.out.println("count = " + resStream.count());
}

    public static void main(String[] args) {
        evenNumbers();
        elementsAreSquared();
        setSurnamesEmployee();

    }
}
